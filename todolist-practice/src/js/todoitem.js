
var defineClass = require('tui-code-snippet').defineClass;
var TodoItem = defineClass({

    /**
     * initialize
     * @param {string} todoname - todo name
     * @param {number} seq - lastSeq of list
     */
    init: function(todoname, seq) {
        this.seq = seq;
        this.todoname = todoname;
        this.createDt = new Date().getTime();
        this.complete = false;
    },
    /**
     * toggle complete state
     */
    toggleComplete: function() {
        this.complete = !this.complete;
    }
});

module.exports = TodoItem;
